import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:test_code/src/application.dart';
import 'package:test_code/src/service_locator.dart' as sl;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await sl.init();
  await GetStorage.init();
  runApp(const MyApp());
}

