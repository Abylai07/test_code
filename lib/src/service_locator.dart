import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:test_code/src/data/datasources/random_data_source.dart';
import 'package:test_code/src/data/repository/random_service_repository.dart';
import 'package:test_code/src/domain/repository/abstract_random_repository.dart';
import 'package:test_code/src/platform/network_info.dart';
import 'package:test_code/src/presentation/bloc/random_data/random_data_cubit.dart';

import 'common/api.dart';
import 'domain/usecase/fetch_data.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  //BLoCs
  sl.registerFactory(() => RandomDataCubit(sl()));

  // UseCases
  sl.registerLazySingleton(() => FetchData(sl()));

  // Remote Repositories
  sl.registerLazySingleton<RandomRemoteDataSource>(
    () => RandomRemoteDataSourceImpl(sl()),
  );

  // Service Repositories
  sl.registerLazySingleton<AbstractRandomRepository>(
    () => RandomServiceRepositoryImpl(sl(), sl()),
  );

  // External
  sl.registerLazySingleton(() => API());
  sl.registerLazySingleton(() => InternetConnectionChecker());
}
