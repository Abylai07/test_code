import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_code/src/presentation/bloc/checkbox_bloc.dart';
import 'package:test_code/src/presentation/bloc/date_bloc.dart';
import 'package:test_code/src/presentation/bloc/random_data/random_data_cubit.dart';
import 'package:test_code/src/presentation/view/home_view.dart';
import 'package:test_code/src/service_locator.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<RandomDataCubit>(
          create: (_) => sl<RandomDataCubit>()..getRandomData(),
        ),
        BlocProvider<CheckboxBloc>(
          create: (_) => CheckboxBloc(),
        ),
        BlocProvider<DateBloc>(
            create: (context) => DateBloc(),
        )
      ],
      child: MaterialApp(
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        home: const HomeView(),
      ),
    );
  }
}
