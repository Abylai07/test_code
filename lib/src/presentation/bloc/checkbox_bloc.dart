import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CheckboxEvent extends Equatable {
  @override
  List<Object?> get props => [];
}


// State
class CheckboxState extends Equatable {
  final bool isChecked;

  const CheckboxState(this.isChecked);

  @override
  List<Object?> get props => [isChecked];
}

// Bloc
class CheckboxBloc extends Bloc<CheckboxEvent, CheckboxState> {
  CheckboxBloc() : super(const CheckboxState(false)) {
    on<CheckboxEvent>((event, emit) {
      emit(CheckboxState(!state.isChecked));
    });
  }
}