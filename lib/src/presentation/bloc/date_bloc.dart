import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class DateEvent extends Equatable {
  final DateTime selectedDate;

  const DateEvent(this.selectedDate);
  @override
  List<Object?> get props => [selectedDate];
}


// State
class DateState extends Equatable {
  final String date;

  const DateState(this.date);

  @override
  List<Object?> get props => [date];
}

// Bloc
class DateBloc extends Bloc<DateEvent, DateState> {
  DateBloc() : super(const DateState('')) {
    on<DateEvent>((event, emit) {
      String newDate = DateFormat('yyyy-MM-dd').format(event.selectedDate);
      emit(DateState(newDate));
    });
  }
}