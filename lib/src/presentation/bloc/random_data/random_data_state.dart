import 'package:equatable/equatable.dart';
import 'package:test_code/src/domain/entity/random_entity.dart';

import '../../../common/enums.dart';

class RandomDataState<T> extends Equatable {
  const RandomDataState({
    this.status = CubitStatus.initial,
    this.entity,
    this.count = 0,
    this.errorCode,
    this.json,
    String? message,
  }) : message = message ?? '';

  final CubitStatus status;
  final RandomEntity? entity;
  final Map<String, dynamic>? json;
  final String message;
  final int count;
  final int? errorCode;

  @override
  List<Object?> get props => [
    status,
    entity,
    message,
    json,
    count,
    errorCode,
  ];

  RandomDataState copyWith({
    RandomEntity? entity,
    Map<String, dynamic>? json,
    CubitStatus? status,
    String? message,
    int? count,
    int? errorCode,
  }) {
    return RandomDataState(
      errorCode: errorCode,
      entity: entity ?? this.entity,
      status: status ?? this.status,
      json: json ?? this.json,
      message: message ?? this.message,
      count: count ?? this.count,
    );
  }
}
