import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_code/src/domain/usecase/fetch_data.dart';
import 'package:test_code/src/presentation/bloc/random_data/random_data_state.dart';

import '../../../common/enums.dart';
import '../../../data/models/random_model.dart';

class RandomDataCubit extends Cubit<RandomDataState> {
  RandomDataCubit(this.fetchData) : super(const RandomDataState());

  final FetchData fetchData;

  void getRandomData() async {
    emit(const RandomDataState(status: CubitStatus.loading));

    final failureOrAuth = await fetchData(const DefaultParams());

    emit(
      failureOrAuth.fold(
        (l) => RandomDataState(
          status: CubitStatus.error,
          message: l.message,
        ),

        (r) => RandomDataState(
          status: CubitStatus.success,
          entity: RandomModel.fromJson(r),
          json: r
        ),
      ),
    );
  }
}
