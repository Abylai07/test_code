import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_json_view/flutter_json_view.dart';
import 'package:test_code/src/common/enums.dart';
import 'package:test_code/src/presentation/widget/rendered_part_widget.dart';

import '../bloc/random_data/random_data_cubit.dart';
import '../bloc/random_data/random_data_state.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Random Data'),
          bottom: const TabBar(
            isScrollable: false,
            tabs: [
              Tab(text: 'Raw'),
              Tab(text: 'Rendered'),
            ],
          ),
        ),
        body: BlocBuilder<RandomDataCubit, RandomDataState>(
            builder: (context, state) {
          switch (state.status) {
            case CubitStatus.success:
              return TabBarView(
                children: [
                  JsonView.map(state.json ?? {}),
                  RenderedPartWidget(data: state.entity),
                ],
              );
            case CubitStatus.loading:
              return const Center(
                child: CircularProgressIndicator.adaptive(),
              );
            default:
              return const Text(
                'Server Error',
                style: TextStyle(fontSize: 24),
              );
          }
        }),
        bottomNavigationBar: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.all(12),
                  backgroundColor: Colors.blueAccent),
              onPressed: () {
                context.read<RandomDataCubit>().getRandomData();
              },
              child: const Text(
                'Refresh',
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
