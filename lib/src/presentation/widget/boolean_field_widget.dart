import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/checkbox_bloc.dart';

class BoolFieldWidget extends StatelessWidget {
  const BoolFieldWidget({super.key, required this.label});
  final String label;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CheckboxBloc, CheckboxState>(
      builder: (context, state) {
        return InkWell(
          onTap: (){
            context
                .read<CheckboxBloc>()
                .add(CheckboxEvent());
          },
          child: Row(
            children: [
              Checkbox(
                  value: state.isChecked,
                  onChanged: (val) {
                    context
                        .read<CheckboxBloc>()
                        .add(CheckboxEvent());
                  }),
              Text(
                label,
                style: const TextStyle(fontSize: 18),
              ),
            ],
          ),
        );
      },
    );
  }
}
