import 'package:cr_calendar/cr_calendar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'calendar/date_picker_title_widget.dart';
import 'calendar/picker_day_item_widget.dart';
import 'calendar/week_days_widget.dart';

void showDatePickerAlert(BuildContext context,
    {required Function(DateTime?, DateTime?) onDateRangeSelected,
    bool singleTap = true}) {
  showCrDatePicker(
    context,
    properties: DatePickerProperties(
      pickerMode: singleTap ? TouchMode.singleTap : TouchMode.rangeSelection,
      dayItemBuilder: (properties) =>
          PickerDayItemWidget(properties: properties),
      weekDaysBuilder: (day) => WeekDaysWidget(day: day),
      pickerTitleBuilder: (date) => DatePickerTitle(date: date),
      yearPickerItemBuilder: (year, isPicked) => Container(
        height: 24,
        width: 54,
        decoration: BoxDecoration(
          color: isPicked ? Colors.blueAccent : Colors.white,
          borderRadius: const BorderRadius.all(Radius.circular(8)),
        ),
        child: Center(
          child: Text(year.toString(),
              style: TextStyle(
                  color: isPicked ? Colors.white : Colors.blueAccent,
                  fontSize: 16)),
        ),
      ),
      controlBarTitleBuilder: (date) => Text(
        DateFormat('M/yyyy').format(date),
        style: const TextStyle(
          fontSize: 18,
          color: Colors.blueAccent,
          fontWeight: FontWeight.normal,
        ),
      ),
      okButtonBuilder: (onPress) => ElevatedButton(
          style: ElevatedButton.styleFrom(
              backgroundColor: Colors.blueAccent,
              padding: const EdgeInsets.symmetric(vertical: 12)),
          child: const Text(
            'Select',
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () {
            onPress?.call();
          }),
      cancelButtonBuilder: (onPress) => OutlinedButton(
        onPressed: () {
          Navigator.pop(context);
        },
        style: OutlinedButton.styleFrom(
            padding: const EdgeInsets.symmetric(vertical: 10)),
        child: const Text(
          'Cancel',
          style: TextStyle(
            fontSize: 18,
            color: Colors.blueAccent,
            fontWeight: FontWeight.normal,
          ),
        ),
      ),
      initialPickerDate: DateTime.now(),
      onDateRangeSelected: onDateRangeSelected,
    ),
  );
}
