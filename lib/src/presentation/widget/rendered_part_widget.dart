import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_code/src/domain/entity/random_entity.dart';
import 'package:test_code/src/presentation/bloc/date_bloc.dart';
import 'package:test_code/src/presentation/widget/show_date_picker.dart';

import 'boolean_field_widget.dart';
import 'custom_text_field.dart';

class RenderedPartWidget extends StatelessWidget {
  const RenderedPartWidget({super.key, required this.data});

  final RandomEntity? data;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              data?.name ?? '',
              style: const TextStyle(fontSize: 18),
            ),
          ),
          const Text(
            '* - required',
            style: TextStyle(fontSize: 14),
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.all(16),
            itemCount: data?.fields.length,
            itemBuilder: (context, i) {
              FieldEntity? field = data?.fields[i];
              return Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: field?.type == 'BOOLEAN'
                    ? BoolFieldWidget(
                        label:
                            '${field?.label} ${field?.required ?? false ? '*' : ''}',
                      )
                    : field?.type == 'DATE'
                        ? buildDateField(context, field)
                        : CustomTextField(
                            field: field,
                          ),
              );
            },
          ),
        ],
      ),
    );
  }

  InkWell buildDateField(BuildContext context, FieldEntity? field) {
    return InkWell(
      onTap: () {
        showDatePickerAlert(
          context,
          singleTap: true,
          onDateRangeSelected: (DateTime? start, DateTime? end) {
            if (start != null && end != null) {
              context.read<DateBloc>().add(DateEvent(start));
            }
          },
        );
      },
      child: BlocBuilder<DateBloc, DateState>(
        builder: (context, state) {
          return Container(
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(6)),
            child: Text(
              state.date.isEmpty
                  ? '${field?.label} ${field?.required ?? false ? '*' : ''}'
                  : state.date,
              style: const TextStyle(fontSize: 16),
            ),
          );
        },
      ),
    );
  }
}
