import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../common/utils/date_parser.dart';
import '../../domain/entity/random_entity.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({super.key, required this.field});
  final FieldEntity? field;
  @override
  Widget build(BuildContext context) {
    return TextField(
      key: ValueKey(field?.id),
      minLines: field?.type == 'TEXTAREA' ? 5 : 1,
      decoration: InputDecoration(
        border: const OutlineInputBorder(),
        labelText: '${field?.label} ${field?.required ?? false ? '*' : ''}',
      ),
      inputFormatters: getFormatterByType(field?.type),
      obscureText: field?.type == 'PASSWORD' ? true : false,
    );
  }

  List<TextInputFormatter>? getFormatterByType(String? type) {
    switch (type) {
      case 'INTEGER':
        return [FilteringTextInputFormatter.allow(RegExp(r'[0-9,]'))];
      case 'DECIMAL':
        return [
          FilteringTextInputFormatter.allow(RegExp(r'^\d+(\.\d{0,2})?$')),
        ];
      case 'DATE':
        return [
          AppUtils.dateMaskFormatter,
        ];
      case 'DATE_TIME':
        return [
          AppUtils.dateTimeMaskFormatter,
        ];
      default:
        return [AppUtils.textMaskFormatter];
    }
  }
}
