import 'package:cr_calendar/cr_calendar.dart';
import 'package:flutter/material.dart';


class PickerDayItemWidget extends StatelessWidget {
  const PickerDayItemWidget({Key? key,
    required this.properties,
  }) : super(key: key);

  final DayItemProperties properties;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1 / 1,
      child: Stack(
        children: [
          if (properties.isInRange)
            Row(
              children: [
                Expanded(
                    child: Container(
                        color: properties.isFirstInRange
                            ? Colors.transparent
                            : Colors.blue.withOpacity(0.4))),
                Expanded(
                    child: Container(
                        color: properties.isLastInRange
                            ? Colors.transparent
                            : Colors.blue.withOpacity(0.4))),
              ],
            ),
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: properties.isFirstInRange ||
                      properties.isLastInRange ||
                      properties.isSelected
                  ?  Colors.blue : properties.isCurrentDay ? Colors.cyanAccent
                  : Colors.transparent,
            ),
            child: Center(
              child: Text('${properties.dayNumber}',
                  style: TextStyle(
                    fontSize: 16,
                      color: properties.isInRange || properties.isSelected
                          ? Colors.white : properties.isCurrentDay ? Colors.purpleAccent
                          : Colors.blue
                              .withOpacity(properties.isInMonth ? 1 : 0.5))),
            ),
          ),
        ],
      ),
    );
  }
}
