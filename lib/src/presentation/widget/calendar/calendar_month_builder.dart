import 'package:flutter/material.dart';

class CalendarMonthBuilder extends StatelessWidget {
  const CalendarMonthBuilder({
    super.key,
    required this.now,
    required this.month, required this.year,
  });

  final DateTime now;
  final int month;
  final int year;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            month.toString(),

          ),
          Text(
            year.toString(),
          ),
        ],
      ),
    );
  }
}