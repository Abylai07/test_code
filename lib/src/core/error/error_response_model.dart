class ErrorResponseModel {
  int? status;
  String? message;
  int? timestamp;
  String? path;
  Details? details;
  String? error;

  ErrorResponseModel({
    this.status,
    this.message,
    this.timestamp,
    this.path,
    this.details,
    this.error,
  });

  factory ErrorResponseModel.fromJson(dynamic data, {int? statusCode}) {
    Map<String, dynamic> json = {};

    if (data is Map) json = data as Map<String, dynamic>;
    if (data is String) return ErrorResponseModel(message: data, status: statusCode);

    return ErrorResponseModel(
      status: json['statusCode'] as int?,
      message: json['message'] as String?,
      path: json['path'] as String?,
      error: json['error'] as String?,
      details:
      json['details'] != null ? Details.fromJson(json['details']) : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['statusCode'] = status;
    data['message'] = message;
    data['timestamp'] = timestamp;
    data['error'] = error;
    data['path'] = path;
    if (details != null) {
      data['details'] = details!.toJson();
    }
    return data;
  }
}

class Details {
  Details({this.email});

  String? email;

  factory Details.fromJson(Map<String, dynamic> json) {
    return Details(email: json['email'] as String?);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    return data;
  }
}
