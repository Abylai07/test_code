import 'package:test_code/src/domain/entity/random_entity.dart';

class RandomModel extends RandomEntity {
  const RandomModel({
    required super.id,
    required super.name,
    required super.fields,
  });

  factory RandomModel.fromJson(Map<String, dynamic> json) {
    return RandomModel(
      id: json['id'] as int,
      name: json['name'] as String,
      fields: (json['fields'] as List<dynamic>)
          .map((fieldJson) =>
          FieldModel.fromJson(fieldJson as Map<String, dynamic>))
          .toList(),
    );
  }
}

class FieldModel extends FieldEntity {
  const FieldModel({
    required super.id,
    required super.label,
    required super.type,
    required super.required,
  });

  factory FieldModel.fromJson(Map<String, dynamic> json) {
    return FieldModel(
      id: json['id'] as String,
      label: json['label'] as String,
      type: json['type'] as String,
      required: json['required'] as bool,
    );
  }

}
