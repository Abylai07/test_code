import 'package:dio/dio.dart';
import 'package:test_code/src/data/models/random_model.dart';
import 'package:test_code/src/domain/usecase/fetch_data.dart';

import '../../common/api.dart';
import '../../core/error/error_response_model.dart';
import '../../core/error/exception.dart';
import '../../common/constants.dart' as constants;

abstract class RandomRemoteDataSource {
  Future<Map<String, dynamic>> fetchRandom();
}

class RandomRemoteDataSourceImpl implements RandomRemoteDataSource {
  final API api;

  RandomRemoteDataSourceImpl(this.api);

  @override
  Future<Map<String, dynamic>> fetchRandom() async {
    try {
      final response = await api.dio.get(
        '${constants.host}forms',
      );
      if (response.statusCode == 200) {
        return response.data;
      } else {
        throw ServerException();
      }
    } on DioError catch (e) {
      if (e.response?.statusCode != null
          ? e.response!.statusCode! >= 500
          : 500 >= 500) {
        throw ServerException(messageError: e.error.toString());
      } else {
        final err = ErrorResponseModel.fromJson(e.response?.data);
        throw CacheException(messageError: err.message);
      }
    }
  }

}