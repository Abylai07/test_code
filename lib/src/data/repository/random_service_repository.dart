import 'package:dartz/dartz.dart';
import 'package:test_code/src/domain/entity/random_entity.dart';
import 'package:test_code/src/domain/repository/abstract_random_repository.dart';

import '../../core/error/exception.dart';
import '../../core/error/failure.dart';
import '../../platform/network_info.dart';
import '../datasources/random_data_source.dart';

class RandomServiceRepositoryImpl extends AbstractRandomRepository{

  RandomServiceRepositoryImpl(this.dataSource, this.networkInfo);
  final RandomRemoteDataSource dataSource;
  final NetworkInfo networkInfo;

  @override
  Future<Either<Failure, Map<String, dynamic>>> fetchData() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await dataSource.fetchRandom();
        return Right(data);
      } on ServerException catch (e) {
        return Left(ServerFailure(e.messageError));
      } on CacheException catch (e) {
        return Left(CacheFailure(e.messageError));
      }
    } else {
      return const Left(ServerFailure());
    }
  }

}
