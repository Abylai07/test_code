import 'package:equatable/equatable.dart';

class RandomEntity extends Equatable {
  final int id;
  final String name;
  final List<FieldEntity> fields;

  const RandomEntity({
    required this.id,
    required this.name,
    required this.fields,
  });

  @override
  List<Object?> get props => [id, name, fields];
}

class FieldEntity extends Equatable {
  final String id;
  final String label;
  final String type;
  final bool required;

  const FieldEntity({
    required this.id,
    required this.label,
    required this.type,
    required this.required,
  });

  @override
  List<Object?> get props => [id, label, type, required];
}