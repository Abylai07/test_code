import 'package:dartz/dartz.dart';

import '../../core/error/failure.dart';

abstract class AbstractRandomRepository {

  Future<Either<Failure, Map<String, dynamic>>> fetchData();
}