import 'package:dartz/dartz.dart';
import 'package:test_code/src/domain/entity/random_entity.dart';

import '../../core/error/failure.dart';
import '../../core/usecases/usecase.dart';
import '../repository/abstract_random_repository.dart';

class FetchData extends UseCase<Map<String, dynamic>?, DefaultParams?> {
  FetchData(this.repository);

  final AbstractRandomRepository repository;

  @override
  Future<Either<Failure, Map<String, dynamic>>> call(DefaultParams? params) async {
    return await repository.fetchData();
  }
}

class DefaultParams {
  const DefaultParams();
}
